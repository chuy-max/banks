from decimal import Decimal


class Money(Decimal):
    def __str__(self):
        return format(self, ',.2f')

    def __repr__(self):
        return format(self, ',.2f')

    def __format__(self, spec):
        spec = spec or ',.2f'
        return '$' + super().__format__(spec)

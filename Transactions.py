import datetime
from Time import Time


class Transactions(list):
    def total(self):
        return self._sum(self)

    def total_positive(self):
        positives_list = self.get_positive_transactions()
        return self._sum(positives_list)

    def total_negative(self):
        negatives_list = self.get_negative_transactions()
        return self._sum(negatives_list)

    def _sum(self, list):
        return sum(t.amount for t in list)

    def get_positive_transactions(self):
        return Transactions(filter(lambda x: x.amount > 0, self))

    def get_negative_transactions(self):
        return Transactions(filter(lambda x: x.amount < 0, self))

    def get_current_month_transactions(self):
        date = datetime.date.today()
        return self._get_month_transactions(date)

    def get_current_day_transactions(self):
        date = datetime.date.today()
        return self.get_transactions_by_date_range(date, date)

    def get_month_transactions(self, month, year):
        date = datetime.date(year, month, 1)
        return self._get_month_transactions(date)

    def _get_month_transactions(self, date):
        first_day = Time.first_day_of_month(date)
        last_day = Time.last_day_of_month(date)
        return self.get_transactions_by_date_range(first_day, last_day)

    def get_transactions_by_date_range(self, first_day, last_day):
        return Transactions(filter(lambda x: first_day <= x.date <= last_day, self))

    def __str__(self):
        s = ''
        for t in self:
            s += str(t)+'\n'
        return s

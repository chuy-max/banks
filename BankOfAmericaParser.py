from bs4 import BeautifulSoup
from Transaction import Transaction
from Transactions import Transactions
from Money import Money
import datetime


class BankOfAmericaParser:
    def get_account_links(self, accounts_html):
        account_links = {}
        soup = BeautifulSoup(accounts_html, 'html.parser')
        for account_li in soup.select('ul.AccountItems li'):
            account_name = account_li.select('span.AccountName a')[0].text
            account_link = account_li.select('span.AccountName a')[0]['href']
            account_links[account_name] = account_link
        return account_links

    def get_debit_transactions(self, debit_account_html):
        transactions = Transactions()
        soup = BeautifulSoup(debit_account_html, 'html.parser')
        self._append_debit_transactions_from_records(transactions, self._get_pending_debit_tr_records(soup), True)
        self._append_debit_transactions_from_records(transactions, self._get_debit_tr_records(soup))
        return transactions

    def _append_debit_transactions_from_records(self, transactions, tr_records, pending=False):
        for record_tr in tr_records:
            t = Transaction()
            t.date = self._get_debit_date(record_tr, pending)
            t.description = self._get_debit_description(record_tr, pending)
            t.amount = self._get_debit_amount(record_tr)
            t.type = self._get_debit_type(record_tr)
            transactions.append(t)

    def _get_pending_debit_tr_records(self, soup):
        return soup.select('table.transaction-records tbody tr.in-transit-record')

    def _get_debit_tr_records(self, soup):
        return soup.select('table.transaction-records tbody tr.record')

    def _get_debit_date(self, record_tr, pending):
        if pending:
            date = datetime.date.today()
        else:
            date_str = record_tr.select('td.date-action span:nth-of-type(2)')[0].text
            date = datetime.datetime.strptime(date_str, "%m/%d/%Y").date()
        return date

    def _get_debit_description(self, record_tr, pending):
        if pending:
            return record_tr.select('td.description')[0].text
        else:
            return record_tr.select('td.description span.transTitleForEditDesc')[0].text

    def _get_debit_amount(self, record_tr):
        amount_str = record_tr.select('td.amount')[0].text.replace(',', '')
        return Money(amount_str)

    def _get_debit_type(self, record_tr):
        return record_tr.select('td.type')[0]['title']
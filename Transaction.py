class Transaction:
    def __init__(self):
        self.date = None
        self.description = None
        self.type = None
        self.amount = None

    def __str__(self):
        # CSV friendly
        return "{0},{1},{2},{3}".format(
            self.date,
            self.description,
            str(self.amount),
            self.type)

    def get_description(self):
        return self.description

    def get_amount(self):
        return self.amount
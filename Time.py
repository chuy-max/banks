import calendar


class Time:
    @staticmethod
    def first_day_of_month(date):
        return date.replace(day=1)

    @staticmethod
    def last_day_of_month(date):
        last_day = calendar.monthrange(date.year, date.month)[1]
        return date.replace(day=last_day)

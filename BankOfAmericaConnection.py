from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from BankOfAmericaParser import BankOfAmericaParser
import time


class BankOfAmericaConnection:
    def __init__(self):
        self.parser = BankOfAmericaParser()

    def connect(self, user, password, answerMap):
        self._get_driver()
        self.driver.get("https://www.bankofamerica.com")
        self._log_in(user, password)
        self._verify_identity(answerMap)
        self._fetch_account_links()

    def close(self):
        self.driver.close()
        self.driver.quit()

    def _get_driver(self):
        chrome_options = Options()
        chrome_options.add_argument("--start-maximized")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('window-size=1920,1080');
        self.driver = webdriver.Chrome(chrome_options=chrome_options)

    def _log_in(self, user, password):
        self._send_keys_to_element("onlineId1", user)
        self._send_keys_to_element("passcode1", password)
        self._click_element("signIn")

    def _send_keys_to_element(self, id, keys):
        self._delay_interaction()
        self.driver.find_element_by_id(id).send_keys(keys)

    def _click_element(self, id):
        self._delay_interaction()
        self.driver.find_element_by_id(id).click()

    def _verify_identity(self, answerMap):
        question_list = self.driver.find_elements_by_css_selector('[for="tlpvt-challenge-answer"]')
        if len(question_list) > 0:
            question = question_list[0].text
            answer = answerMap[question]

            self._select_dont_remember_computer()
            self._send_keys_to_element("tlpvt-challenge-answer", answer)
            self._click_element("verify-cq-submit")

    def _select_dont_remember_computer(self):
        try:
            self.driver.find_element_by_id("no-recognize").click()
        except NoSuchElementException:
            pass

    def _delay_interaction(self):
        time.sleep(1)

    def _fetch_account_links(self):
        html_doc = self.driver.page_source
        self.account_links = self.parser.get_account_links(html_doc)

    def get_account_transactions(self, account):
        self.driver.get("https://secure.bankofamerica.com" + self.account_links[account])
        html_doc = self.driver.page_source
        transactions = self.parser.get_debit_transactions(html_doc)
        return transactions
